    $ git diff
    zeigt änderungen zw. getrackten datei und  änderungen, die noch nicht gestaged wurden
    $ git diff --staged
    änderungen im stage zustande, aber noch nicht comitted
    $ git diff test.txt
    änderungen nur für datei test.txt, die noch nicht gestaged wurden
    $ git diff --theirs
    bei einem merge werden die änderungen des remote/origin/master angezeigt??
    $ git diff --ours
    bei einem merge werden die änderungen des lokalen gits angezeigt??
    $ git log
    zeigt alle commits an
    $ git log --oneline
    zeigt die commits in einer zeile an
    $ git log --oneline --all
    zusätzliche anzeige der remote commits
    $ git log --oneline --all --graph
    zusätzlich anzeige mit baum
    $ git log --oneline --all --graph --decorate
    kein unterschied erkennbar
    $ git log --follow -p -- filename
    erkennt, dass datei umbenannt wurde und zeigt zusätzlich die änderungen unter altem dateinamen an
    $ git log -S'static void Main'
    sucht in der historie des codes / der comitteten deltas nach static void Main 
    $ git log --pretty=format:"%h - %an, %ar : %s"
    ausführlichere infos zum commit: autor, zeitangabe in einer zeile